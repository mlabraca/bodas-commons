//
//  LinkableTextView.swift
//  Wedshoots
//
//  Created by Miguel Angel Labraca on 15/05/2018.
//  Copyright © 2018 Egle Systems S.L. All rights reserved.
//

import UIKit

@objc(WSLinkType)
public enum LinkType: Int {
    case legal
    case privacy
    
    var name: String {
        switch self {
        case .legal: return "/legal"
        case .privacy: return "/privacy"
        }
    }
    
    static func linkWithPath(path: String) -> LinkType? {
        switch path {
        case LinkType.legal.name:
            return LinkType.legal
        case LinkType.privacy.name:
            return LinkType.privacy
        default:
            return nil
        }
    }
}

@objc(WSLinkableTextViewDelegate)
public protocol LinkableTextViewDelegate: class {
    func didClickRegisteredType(_ sender: LinkableTextView, linkType: LinkType)
    func didClickURL(_ sender: LinkableTextView, url: URL)
}

@objc(WSLinkableTextView)
public class LinkableTextView: UITextView, UITextViewDelegate {
    
    @objc
    public weak var linkableDelegate: LinkableTextViewDelegate?
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    public override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        commonInit()
    }
    
    private func commonInit() {
        self.isEditable = false
        self.dataDetectorTypes = .link
        self.delegate = self
        self.isSelectable = true
        self.isUserInteractionEnabled = true
        self.isScrollEnabled = false
    }
    
    // MARK: - Public methods
    
    @objc
    public func setLinkable(text: String) {
        let currentFont = self.font
        let currentColor = self.textColor
        let currentTintColor = self.tintColor
        
        self.attributedText = text.html2Attributed
        
        self.font = currentFont
        self.textColor = currentColor
        self.tintColor = currentTintColor
    }
    
    // MARK: - UITextViewDelegate
    
    public func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        if let linkType = LinkType.linkWithPath(path: URL.path) {
            self.linkableDelegate?.didClickRegisteredType(self, linkType: linkType)
        } else {
            self.linkableDelegate?.didClickURL(self, url: URL)
        }
        
        return false
    }
    
}

private extension String {
    var html2Attributed: NSAttributedString? {
        do {
            guard let data = data(using: String.Encoding.utf8) else {
                return nil
            }
            return try NSAttributedString(data: data,
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        } catch {
            print("error: ", error)
            return nil
        }
    }
}
