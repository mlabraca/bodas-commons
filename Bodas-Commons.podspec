#
# Be sure to run `pod lib lint Bodas-Commons.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'Bodas-Commons'
  s.version          = '0.1.0'
  s.summary          = 'All common files for Bodas apps.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://bitbucket.org/mlabraca/bodas-commons'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'PRIVATE', :text => 'Internal use framework' }
  s.author           = { 'Miguel Angel Labraca' => 'mlabraca@bodas.net' }
  s.source           = { :git => 'https://bitbucket.org/mlabraca/bodas-commons.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'
  s.swift_version = '4.0'

  #s.source_files = 'Bodas-Commons/Classes/**/*'
  
  # s.resource_bundles = {
  #   'Bodas-Commons' => ['Bodas-Commons/Assets/*.png']
  # }

  # Needed for the libraries that are static, like FirebaseCore
  s.static_framework = true

  s.subspec 'default' do |ss|    
    ss.dependency 'Bodas-Commons/Analytics'
    ss.dependency 'Bodas-Commons/TestAB'
    ss.dependency 'Bodas-Commons/UIComponents'
    ss.dependency 'Bodas-Commons/Utilities'
    # External dependencies
    #ss.dependency 'Fabric'
    #ss.dependency 'FBSDKCoreKit'
    #ss.dependency 'FBSDKLoginKit'
    #ss.dependency 'Crashlytics'
  end

  s.subspec 'Analytics' do |ss|
    ss.source_files = 'Bodas-Commons/Classes/Analytics/*.{swift}'
    # External dependencies
    ss.dependency 'AppsFlyer-SDK', '4.5.6'
  end

  s.subspec 'TestAB' do |ss|
    ss.source_files = 'Bodas-Commons/Classes/TestAB/*.{swift}'
    # External dependencies
    ss.dependency 'Firebase/Core'
    ss.dependency 'Firebase/RemoteConfig'
  end

  s.subspec 'UIComponents' do |ss|
    ss.source_files = 'Bodas-Commons/Classes/UIComponents/*.{swift}'
  end

  s.subspec 'Utilities' do |ss|
    ss.source_files = 'Bodas-Commons/Classes/Utilities/*.{swift}'
  end

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
