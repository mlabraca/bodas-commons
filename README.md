# Bodas-Commons

[![CI Status](https://img.shields.io/travis/Miguel Angel Labraca/Bodas-Commons.svg?style=flat)](https://travis-ci.org/Miguel Angel Labraca/Bodas-Commons)
[![Version](https://img.shields.io/cocoapods/v/Bodas-Commons.svg?style=flat)](https://cocoapods.org/pods/Bodas-Commons)
[![License](https://img.shields.io/cocoapods/l/Bodas-Commons.svg?style=flat)](https://cocoapods.org/pods/Bodas-Commons)
[![Platform](https://img.shields.io/cocoapods/p/Bodas-Commons.svg?style=flat)](https://cocoapods.org/pods/Bodas-Commons)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

Bodas-Commons is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'Bodas-Commons'
```

## Author

Miguel Angel Labraca, mlabraca@bodas.net

## License

Bodas-Commons is available under the MIT license. See the LICENSE file for more info.
